/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>

// Estrutura de um nodo: pode representar uma raiz, nodo intermediário ou
// folha, dependendo se possui irmãos, filhos ou pai.
typedef struct comp_tree_t comp_tree_t;



// Assinaturas das funções

void create_code(comp_tree_t* node);
comp_tree_t* createNode(int data);
void appendChild(comp_tree_t* newFather, comp_tree_t* newChild);
void safelyRemoveNode_DiscardingChildren(comp_tree_t* removableNode);
comp_tree_t* safelyRemoveNode_AttachingChildrenToFather(comp_tree_t* removableNode);
void freeNodeAndChildren(comp_tree_t* father);
void safelyRemoveNode(comp_tree_t* removableNode);
void freeNodeAndChildren(comp_tree_t* father);
void tests_tree();
