/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>

#include "comp_tree.h"

typedef struct comp_list_t comp_list_t;

struct comp_list_t {
    int list_value;
    comp_list_t* list_next;
};


comp_list_t* list_create (void);
comp_list_t* list_insert (comp_list_t* l, int i);
comp_list_t* list_remove (comp_list_t* l, int i);
comp_list_t* list_concatenate (comp_list_t* l1, comp_list_t* l2);
void list_destroy (comp_list_t* l);
int search_list (comp_list_t* l, int i);
void print_list (comp_list_t* l);
