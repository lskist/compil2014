/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>

typedef struct comp_dict_item_t comp_dict_item_t;
typedef struct comp_dict_t comp_dict_t;

struct comp_dict_item_t {
    char* key;
    char* value;
};

struct comp_dict_t {
    comp_dict_item_t dict_item;
    comp_dict_t* dict_next;
};

comp_dict_t* dict_create (void);
comp_dict_t* dict_insert (comp_dict_t* d, char* k, char* v);
comp_dict_t* dict_remove (comp_dict_t* d, char* k);
void dict_destroy (comp_dict_t* d);
char* search_dict (comp_dict_t* d, char* k);
void print_dict (comp_dict_t* d);

comp_dict_t* symbol_table;
int current_line;
