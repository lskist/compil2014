/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>
#include "comp_list.h"
//#include "iks_ast.h"

// Cria uma nova lista vazia
// Saída: nova lista
comp_list_t* list_create (void) {
    return NULL;
}


// Insere um registro na lista
// Entrada: lista, registro
// Saída: lista com o registro inserido
comp_list_t* list_insert (comp_list_t* l, int i) {
    comp_list_t* new_list = (comp_list_t*) malloc(sizeof(comp_list_t));
    new_list->list_next = l;
    new_list->list_value = i;
    return new_list;
}

// Remove um registro da lista
// Entrada: lista, valor do registro a ser inserido
// Saída: lista sem o registro com aquele valor
comp_list_t* list_remove (comp_list_t* l, int i) {
    comp_list_t* list_previous = NULL;
    comp_list_t* temp_list = l;
    while (temp_list != NULL && temp_list->list_value != i) {
        list_previous = temp_list;
        temp_list = temp_list->list_next;
    }
    if (temp_list == NULL) {
        return l;
    }
    if (list_previous == NULL) {
        l = temp_list->list_next;
    }
    else {
        list_previous->list_next = temp_list->list_next;
    }
    free(temp_list);
    return l;
}

// Concatena duas listas
// Entrada: lista 1, lista 2
// Saída: lista concatenada com a lista 2 adicionado no fim da lista 1
comp_list_t* list_concatenate (comp_list_t* l1, comp_list_t* l2) {
	comp_list_t* temp_list;
    comp_list_t* new_list;

    if (l1 == NULL) {
        return l2;
    }
    temp_list = l1;
    do {
        new_list = temp_list;
        temp_list = temp_list->list_next;
    } while(temp_list != NULL);
    new_list->list_next = l2;
    return l1;
}

// Desaloca e libera a memória ocupada por uma lista
// Entrada: lista a ser destruída
void list_destroy (comp_list_t* l) {
    comp_list_t* temp_list = l;
    while (temp_list != NULL) {
        comp_list_t* list_next_reference = temp_list->list_next;
        free(temp_list);
        temp_list = list_next_reference;
    }
}

// Procura por um dado valor na lista
// Entrada: lista, valor de registro a ser procura
// Saída: 1 se existir registro com aquele valor, 0 caso contrário
int search_list (comp_list_t* l, int i) {
    comp_list_t* temp_list;
    for (temp_list = l; temp_list != NULL; temp_list = temp_list->list_next) {
        if (temp_list->list_value == i) {
            return 1;
        }
    }
    return 0;
}

// Imprime o atual estado de uma lista
// Entrada: lista
void print_list (comp_list_t* l) {
    comp_list_t* temp_list;
    printf("List =");
    for (temp_list = l; temp_list != NULL; temp_list = temp_list->list_next) {
        printf(" %d", temp_list->list_value);
    }
    printf("\n");
}

void tests_list() {
     // Cria a lista e adiciona uns valores
     comp_list_t* lista = list_create();
     lista = list_insert(lista,10);
     lista = list_insert(lista,20);
     lista = list_insert(lista,30);
     
     // Imprime lista
     print_list(lista);
     
     // Procura por valores que existem e não existem
     // Deve retornar 1:
     printf("%i\n",search_list(lista,10));
     printf("%i\n",search_list(lista,30));
     // Deve retornar 0:
     printf("%i\n",search_list(lista,25));
     printf("%i\n",search_list(lista,-1));
     
     // Cria a lista2 e adiciona uns valores
     comp_list_t* lista2 = list_create();
     lista2 = list_insert(lista2,666);
     lista2 = list_insert(lista2,777);
     
     // Imprime lista2
     print_list(lista2);
     
     // Procura por valores que existem e não existem
     // Deve retornar 1:
     printf("%i\n",search_list(lista2,666));
     printf("%i\n",search_list(lista2,777));
     // Deve retornar 0:
     printf("%i\n",search_list(lista2,25));
     printf("%i\n",search_list(lista2,-1));
     
     // Concatena com outra lista
     comp_list_t* concatenada = list_create();
     concatenada = list_concatenate(lista,lista2);
     
     // Procura por valores que existem e não existem
     // Deve retornar 1:
     printf("%i\n",search_list(concatenada,20));
     printf("%i\n",search_list(concatenada,777));
     // Deve retornar 0:
     printf("%i\n",search_list(concatenada,555));
     printf("%i\n",search_list(concatenada,0));    
     
     // Concatena com duas listas vazias (não deve alterar nada)
     comp_list_t* vazia = list_create();
     concatenada = list_concatenate(vazia,concatenada);
     concatenada = list_concatenate(concatenada,NULL);
     // Repete os testes:  
     // Deve retornar 1:
     printf("%i\n",search_list(concatenada,20));
     printf("%i\n",search_list(concatenada,777));
     // Deve retornar 0:
     printf("%i\n",search_list(concatenada,555));
     printf("%i\n",search_list(concatenada,0));       
     
     // Remove alguns registros
     concatenada = list_remove(concatenada,666);
     concatenada = list_remove(concatenada,20);
     // Procura por valores que existem e não existem
     // Deve retornar 1:
     printf("%i\n",search_list(concatenada,10));
     printf("%i\n",search_list(concatenada,777));
     // Deve retornar 0:
     printf("%i\n",search_list(concatenada,666));
     printf("%i\n",search_list(concatenada,-666));
     
     // Imprime estado atual da lista:
     print_list(concatenada);

}

//int main () { tests_list(); }
