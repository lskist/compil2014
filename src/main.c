/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */

#include <stdio.h>
#include "comp_dict.h"
#include "comp_list.h"
#include "comp_tree.h"


int getLineNumber (void)
{
  /* deve ser implementada */
  return current_line;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s\n", mensagem);
}

int main (int argc, char **argv)
{
  //gv_init("./olha.dot");
  int resultado = yyparse();
  if(resultado)
	{
	printf("Saida: %d\n");
	printf("Erro encontrado na linha %d\n", current_line);
	}
  //gv_close();
  return resultado;
}
