/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_dict.h"


// Cria um dicionário vazio
// Retorno: dicionário vazio
comp_dict_t* dict_create (void) {
    return NULL;
}

// Insere um registro no dicionário
// Entrada: dicionário, chave, valor
// Saída: dicionário com o registro inserido
comp_dict_t* dict_insert (comp_dict_t* d, char* k, char* v) {
    comp_dict_t* new_dict = (comp_dict_t*) malloc(sizeof(comp_dict_t));
    new_dict->dict_next = d;
    new_dict->dict_item.key = (char*)calloc(strlen(k)+1,sizeof(char));
	strcpy(new_dict->dict_item.key, k);
    new_dict->dict_item.value = v;
    return new_dict;
}

// Remove um registro do dicionário, caso ele exista, senăo năo faz nada
// Entrada: dicionário, chave a ser removida
// Saída: dicionário com o registro removido
comp_dict_t* dict_remove (comp_dict_t* d, char* k) {
    comp_dict_t* dict_previous = NULL;
    comp_dict_t* temp_dict = d;
    while (temp_dict != NULL && temp_dict->dict_item.key != k) {
        dict_previous = temp_dict;
        temp_dict = temp_dict->dict_next;
    }
    if (temp_dict == NULL) {
        return d;
    }
    if (dict_previous == NULL) {
        d = temp_dict->dict_next;
    }
    else {
        dict_previous->dict_next = temp_dict->dict_next;
    }
    free(temp_dict);
    return d;
}

// Edita o valor de uma chave, caso ela exista, senăo năo faz nada
// Entrada: dicionário, chave a ter seu valor alterado, novo valor
// Saída: dicionário com o registro alterado
comp_dict_t* dict_edit (comp_dict_t* d, char* k, char* v) {
    comp_dict_t* temp_dict;
    for (temp_dict = d; temp_dict != NULL; temp_dict = temp_dict->dict_next) {
        if (temp_dict->dict_item.key == k) {
            temp_dict->dict_item.value = v;
            return d;
        }
    }
    return d;
}

// Desaloca e libera a memória utilizada pelo dicionário
// Entrada: dicionário a ser destruído
void dict_destroy (comp_dict_t* d) {
    comp_dict_t* temp_dict = d;
    while (temp_dict != NULL) {
        comp_dict_t* next_dict_reference = temp_dict->dict_next;
        free(temp_dict);
        temp_dict = next_dict_reference;
    }
}

// Procura pelo valor de uma chave do dicionário
// Entrada: dicionário, chave
// Saída: string valor da chave caso exista, senăo, '\0'
char* search_dict (comp_dict_t* d, char* k) {
    comp_dict_t* temp_dict;
    for (temp_dict = d; temp_dict != NULL; temp_dict = temp_dict->dict_next) {
        if (temp_dict->dict_item.key == k) {
            return temp_dict->dict_item.value;
        }
    }
    return "NULL";
}


// Imprime estado atual do dicionário
// Entrada: dicionário
void print_dict (comp_dict_t* d) {
    comp_dict_t* temp_dict;
	//1 TK_OC_LE [<=]
    for (temp_dict = d; temp_dict != NULL; temp_dict = temp_dict->dict_next) {
        printf("Elemento: %s, linha %s\n", temp_dict->dict_item.key, temp_dict->dict_item.value);
    }
    printf("\n");
}

void tests_dict () {
    // Cria um dicionário e adiciona algumas chaves
    comp_dict_t* dicio = dict_create();
    dicio = dict_insert(dicio,"k_um","v_um");
    dicio = dict_insert(dicio,"chavedois","valordois");
    dicio = dict_insert(dicio,"key_3","value_3");
    
    // Teste de retornar os valores das chaves    
    printf("%s\n",search_dict(dicio,"k_um")); // deve imprimir "v_um"
    printf("%s\n",search_dict(dicio,"key_3")); // deve imprimir "value_3"
    printf("%s\n",search_dict(dicio,"etj839")); // deve imprimir (null)
    printf("%s\n",search_dict(dicio,"valordois")); // deve imprimir (null)
    
    // Remove a primeira entrada e tenta imprimir
    dicio = dict_remove(dicio,"k_um");
    printf("%s\n",search_dict(dicio,"k_um")); // deve imprimir (null)
    
    // Edita a segunda entrada e imprime
    dicio = dict_edit(dicio,"chavedois","valor editado!");
    printf("%s\n",search_dict(dicio,"chavedois")); // deve imprimir (null)
    
    // Imprime estado atual do dicionário
    printf("\n");
    print_dict(dicio);
    printf("\n");
            
}

//int main () { testes(); }
