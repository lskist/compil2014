/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_tree.h"
#include "comp_list.h"
#include "iks_ast.h"
#include "gv.h"

struct comp_tree_t {
    comp_tree_t* olderSibling; 
    comp_tree_t* youngerSibling;
    comp_tree_t* youngestChild; 
    comp_tree_t* father;
    int type;
}; 

void create_code(comp_tree_t* node)
{
    comp_tree_t* p;
    for (p = node->youngestChild; p != NULL; p = p->olderSibling)
    {
        create_code(p);
    }
    if(node->type == IKS_AST_ARIM_SOMA)
    {
        printf("a+");
    }
    return;
}


void typeToString(int type) {
    if( type == IKS_AST_PROGRAMA )
        printf("IKS_AST_PROGRAMA\n");
    if( type == IKS_AST_FUNCAO )
        printf("IKS_AST_FUNCAO\n");
    if( type == IKS_AST_IF_ELSE )
        printf("IKS_AST_IF_ELSE\n");
    if( type == IKS_AST_DO_WHILE )
        printf("IKS_AST_DO_WHILE\n");
    if( type == IKS_AST_WHILE_DO )
        printf("IKS_AST_WHILE_DO\n");
    if( type == IKS_AST_INPUT )
        printf("IKS_AST_INPUT\n");
    if( type == IKS_AST_OUTPUT )
        printf("IKS_AST_OUTPUT\n");
    if( type == IKS_AST_ATRIBUICAO )
        printf("IKS_AST_ATRIBUICAO\n");
    if( type == IKS_AST_RETURN )
        printf("IKS_AST_RETURN\n");
    if( type == IKS_AST_BLOCO )
        printf("IKS_AST_BLOCO \n");
    if( type == IKS_AST_IDENTIFICADOR )
        printf("IKS_AST_IDENTI\n");
    if( type == IKS_AST_LITERAL )
        printf("IKS_AST_LITERA\n");
    if( type == IKS_AST_ARIM_SOMA )
        printf("IKS_AST_ARIM_SOMA\n");
    if( type == IKS_AST_ARIM_SUBTRACAO )
        printf("IKS_AST_ARIM_SUBTRACAO\n");
    if( type == IKS_AST_ARIM_MULTIPLICACAO)
        printf("IKS_AST_ARIM_MULTIPLICACAO\n");
    if( type == IKS_AST_ARIM_DIVISAO    )
        printf("IKS_AST_ARIM_DIVISAO\n");
    if( type == IKS_AST_ARIM_INVERSAO )
        printf("IKS_AST_ARIM_INVERSAO\n");
    if( type == IKS_AST_LOGICO_E  )
        printf("IKS_AST_LOGICO_E\n");
    if( type == IKS_AST_LOGICO_OU  )
        printf("IKS_AST_LOGICO_OU\n");
    if( type == IKS_AST_LOGICO_COMP_DIF  )
        printf("IKS_AST_LOGICO_COMP_DIF\n");
    if( type == IKS_AST_LOGICO_COMP_IGUAL   )
        printf("IKS_AST_LOGICO_COMP_IGUAL\n");
    if( type == IKS_AST_LOGICO_COMP_LE      )
        printf("IKS_AST_LOGICO_COMP_LE\n");
    if( type == IKS_AST_LOGICO_COMP_GE)
        printf("IKS_AST_LOGICO_COMP_GE\n");
    if( type == IKS_AST_LOGICO_COMP_L )
        printf("IKS_AST_LOGICO_COMP_L\n");
    if( type == IKS_AST_LOGICO_COMP_G )
        printf("IKS_AST_LOGICO_COMP_G\n");
    if( type == IKS_AST_LOGICO_COMP_NEGACAO )
        printf("IKS_AST_LOGICO_COMP_NEGACAO \n");
    if( type == IKS_AST_VETOR_INDEXADO )
        printf("IKS_AST_VETOR_INDEXADO\n");
    if( type == IKS_AST_CHAMADA_DE_FUNCAO )
        printf("IKS_AST_CHAMADA_DE_FUNCAO\n");
    /*if(type == TK_PR_INT )
	    printf("TK_PR_INT\n");
    if(type == TK_PR_FLOAT )
	    printf("TK_PR_FLOAT\n");
    if(type == TK_PR_BOOL )
	    printf("TK_PR_BOOL\n");
    if(type == TK_PR_CHAR )
	    printf("TK_PR_CHAR\n");
    if(type == TK_PR_STRING )
	    printf("TK_PR_STRING\n");
    if(type == TK_PR_IF )
	    printf("TK_PR_IF\n");
    if(type == TK_PR_THEN )
	    printf("TK_PR_THEN\n");
    if(type == TK_PR_ELSE )
	    printf("TK_PR_ELSE\n");
    if(type == TK_PR_WHILE )
	    printf("TK_PR_WHILE\n");
    if(type == TK_PR_DO )
	    printf("TK_PR_DO\n");
    if(type == TK_PR_INPUT )
	    printf("TK_PR_INPUT\n");
    if(type == TK_PR_OUTPUT )
	    printf("TK_PR_OUTPUT\n");
    if(type == TK_PR_RETURN )
	    printf("TK_PR_RETURN\n");
    if(type == TK_OC_LE )
	    printf("TK_OC_LE\n");
    if(type == TK_OC_GE )
	    printf("TK_OC_GE\n");
    if(type == TK_OC_EQ )
	    printf("TK_OC_EQ\n");
    if(type == TK_OC_NE )
	    printf("TK_OC_NE\n");
    if(type == TK_OC_AND )
	    printf("TK_OC_AND\n");
    if(type == TK_OC_OR )
	    printf("TK_OC_OR\n");
    if(type == TK_LIT_INT )
	    printf("TK_LIT_INT\n");
    if(type == TK_LIT_FLOAT )
	    printf("TK_LIT_FLOAT\n");
    if(type == TK_LIT_FALSE )
	    printf("TK_LIT_FALSE\n");
    if(type == TK_LIT_TRUE )
	    printf("TK_LIT_TRUE\n");
    if(type == TK_LIT_CHAR )
	    printf("TK_LIT_CHAR\n");
    if(type == TK_LIT_STRING )
	    printf("TK_LIT_STRING\n");
    if(type == TK_IDENTIFICADOR )
	    printf("TK_IDENTIFICADOR\n");
    if(type == TOKEN_ERRO )
	    printf("TOKEN_ERRO\n"); */
}


const void* createGVT(comp_tree_t* node)
{
	const void* newGVNode;
	comp_tree_t* p;
	if(node == NULL)
		return NULL;
	const void *gvNode = (const void*)malloc(sizeof(const void));
    switch(node->type){
    case IKS_AST_IDENTIFICADOR:
        gv_declare(node->type, gvNode, "var ident");
        break;
    case IKS_AST_LITERAL:
        gv_declare(node->type, gvNode, "var lit");
        break;
    case IKS_AST_FUNCAO:
        gv_declare(node->type, gvNode, "var func");
        break;
    default:
        gv_declare(node->type, gvNode, NULL);
        break;
    }
    
    for (p = node->youngestChild; p != NULL; p = p->olderSibling) {
        newGVNode = createGVT(p);
	printf("sss");
        gv_connect(gvNode, newGVNode);
	}
    return gvNode;    
}


void printPretty(int depth, comp_tree_t* node)
{
    comp_tree_t* p;
    int i;
    for(i = depth; i > 0; i--)
        printf("  ");
    if(node->olderSibling == NULL)
        printf("\\-");
    else
        printf("|-");
    typeToString(node->type);   
    depth++;
    for (p = node->youngestChild; p != NULL; p = p->olderSibling) 
        printPretty(depth, p);
}

void printTree(comp_tree_t* node) {
    printPretty(0, node); 
}

//cria arvore de sintaxe abstrata
comp_tree_t* astCreate(int type, comp_tree_t* first, comp_tree_t* second, comp_tree_t* third, comp_tree_t* fourth) {
    comp_tree_t* current;
    //typeToString(type);
    current = createNode(type);
    if(first != NULL) {
        appendChild(current,first);  
    }
    if(second != NULL) {
        appendChild(current,second); 
    }
    if(third != NULL) {
        appendChild(current,third);
    }
    if(fourth != NULL) {
        appendChild(current,fourth);
    }
    return current;
}

// Cria um novo nodo com um dado especificado.
// Entrada: dado
// Saída: nodo com o dado
comp_tree_t* createNode(int data) {
    comp_tree_t* newNode = (comp_tree_t*)malloc(sizeof(comp_tree_t));
    newNode->youngerSibling = NULL;
    newNode->olderSibling = NULL;
    newNode->youngestChild = NULL;
    newNode->father = NULL;
    newNode->type = data;
    return newNode;
}

// Aplica a função nativa de liberação de memória free() recursivamente sobre
// todos os nodos filhos de um dado nodo, além do próprio.
// Entrada: nodo ter a si e seus filhos desalocados.
void checkDeclaration(comp_tree_t* father) {
     if (father->youngestChild != NULL)
          freeNodeAndChildren(father->youngestChild);          
     if (father->olderSibling != NULL)
          freeNodeAndChildren(father->olderSibling);
     free(father);
}

// Torna um nodo pai de um outro nodo específico, já inicializado. Esse novo
// pai se torna avô dos filhos do outro nodo, e assim por diante.
// O nodo filho é desvencilhado seguramente (mantendo consistência com pais
// e irmãos) de sua posição atual na árvore antes de ser ligado ao novo pai.
// Entrada: nodo a se tornar pai, nodo a se tornar filho
void appendChild(comp_tree_t* newFather, comp_tree_t* newChild) {
    safelyRemoveNode(newChild);
    comp_tree_t* youngestChild_before = newFather->youngestChild;    
    newFather->youngestChild = newChild;
    newChild->father = newFather;
    newChild->youngerSibling = NULL;
    if (youngestChild_before == NULL) {        
        newChild->olderSibling = NULL;
    }
    else {
        newChild->olderSibling = youngestChild_before;
        youngestChild_before->youngerSibling = newChild;
    }
}


// Remove um nodo de uma árvore de maneira segura (garantindo consistência dos
// pais e irmãos) e descarta os filhos.
// Entrada: nodo a ser removido.
void safelyRemoveNode_DiscardingChildren(comp_tree_t* removableNode) {
    safelyRemoveNode(removableNode);
    freeNodeAndChildren(removableNode);
}

// Remove um nodo de uma árvore de maneira segura (garantindo consistência dos
// pais e irmãos) e liga os filhos órfãos ao pai do nodo removido.
// Entrada: nodo a ser removido.
// Saída: o primeiro (mais novo) filho, caso exista, do nodo removido. Senão,
//        NULL.
comp_tree_t* safelyRemoveNode_AttachingChildrenToFather(comp_tree_t* removableNode) {
    comp_tree_t* youngestChild = removableNode->youngestChild;
    if (removableNode->father == NULL && removableNode->youngestChild != NULL) {
        comp_tree_t* bufferNode = removableNode->youngestChild;
        bufferNode->father = NULL;
        while (bufferNode->olderSibling != NULL) {
            bufferNode = bufferNode->olderSibling;
            bufferNode->father == NULL;
        }
    }
    else if (removableNode->youngestChild == NULL) {
        safelyRemoveNode(removableNode);
    }
    else { // se ele tem filho(s) e tem pai
        safelyRemoveNode(removableNode);
        comp_tree_t* bufferNode = removableNode->youngestChild;
        comp_tree_t* nextNode = bufferNode->olderSibling;
        bufferNode->father = removableNode->father;
        bufferNode->youngerSibling = removableNode->youngerSibling;
        if (bufferNode->youngerSibling != NULL)
            bufferNode->youngerSibling->olderSibling = bufferNode;
        while (nextNode != NULL) {
            bufferNode = nextNode;
            nextNode = nextNode->olderSibling;
            bufferNode->father = removableNode->father;
        }
        bufferNode->olderSibling = removableNode->olderSibling;
        if (bufferNode->olderSibling != NULL)
            bufferNode->olderSibling->youngerSibling = bufferNode;
        if (removableNode->youngerSibling == NULL)
           removableNode->father->youngestChild = removableNode-> youngestChild;
    }
    free(removableNode); 
    return youngestChild;
}

// Aplica a função nativa de liberação de memória free() recursivamente sobre
// todos os nodos filhos de um dado nodo, além do próprio.
// Entrada: nodo ter a si e seus filhos desalocados.
void freeNodeAndChildren(comp_tree_t* father) {
     if (father->youngestChild != NULL)
          freeNodeAndChildren(father->youngestChild);          
     if (father->olderSibling != NULL)
          freeNodeAndChildren(father->olderSibling);
     free(father);
}

// Funções "private":
void safelyRemoveNode(comp_tree_t* removableNode) {
    if ((removableNode->father != NULL) && (removableNode->olderSibling == NULL && removableNode->youngerSibling == NULL)) {
        removableNode->father->youngestChild = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling != NULL && removableNode->youngerSibling == NULL)) {
        removableNode->father->youngestChild = removableNode->olderSibling;
        removableNode->olderSibling->youngerSibling = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling == NULL && removableNode->youngerSibling != NULL)) {
        removableNode->youngerSibling->olderSibling = NULL;
    }
    else if ((removableNode->father != NULL) && (removableNode->olderSibling != NULL && removableNode->youngerSibling != NULL)) {
        removableNode->olderSibling->youngerSibling = removableNode->youngerSibling;   
        removableNode->youngerSibling->olderSibling = removableNode->olderSibling;
    }  
}

