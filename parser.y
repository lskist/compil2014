/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */

%{
#include <stdio.h>
#include <stdlib.h>
#include "comp_tree.h"
#include "comp_list.h"
#include "iks_ast.h"
#include "errors.h"

%}


%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%left TK_OC_OR TK_OC_AND
%left '<' '>' TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
%left '+' '-'
%left '*' '/'
%%


 /* Regras (e ações) da gramática da Linguagem K */



p : program {  $$ = astCreate(IKS_AST_PROGRAMA, $1, NULL, NULL, NULL); printTree($$); create_code($$);/*const void* GVTree = createGVT($$);*/  }

program : global_declaration program    { $$ =  astCreate(IKS_AST_FUNCAO, $1, $2, NULL, NULL); }
	| function_declaration program  { $$ =  astCreate(IKS_AST_FUNCAO, $1, $2, NULL, NULL); }
	|
	;

global_declaration : variable_declaration ';'   			{ $$ = $1 }
	| vector_declaration ';'					{ $$ = $1 }
	;

variable_declaration :  type TK_IDENTIFICADOR  { $$ = $1; }
	;

vector_declaration : type TK_IDENTIFICADOR '[' TK_LIT_INT ']'  { $$ = $1 }
	;

type : TK_PR_INT  	  { $$ = $1 }
	| TK_PR_FLOAT     { $$ = $1 }
	| TK_PR_BOOL	  { $$ = $1 }
	| TK_PR_CHAR	  { $$ = $1 }
	| TK_PR_STRING	  { $$ = $1 }
	;

function_declaration : header local_declaration command_block  { $$ = astCreate(IKS_AST_FUNCAO, $1, $2, $3, NULL); }
	;

header : type TK_IDENTIFICADOR'(' param_list ')'
	;

param_list : param_list_not_empty  { $$ = $1 }
	|
	;

param_list_not_empty : param ',' param_list_not_empty
	| param
	;

param : type TK_IDENTIFICADOR
	;

local_declaration : variable_declaration ';' local_declaration  { $$ = astCreate(IKS_AST_BLOCO, $1, $3, NULL, NULL); }
	|	
	;

command_block : '{' seq_command '}'			{ $$ = $2 }
	;

seq_command : seq_command_not_empty			{ $$ = $1 }
	|
	;

seq_command_not_empty : command ';' seq_command_not_empty  { $$ = astCreate(IKS_AST_BLOCO, $1, $3, NULL, NULL); }
	| command  { $$ = $1 }
	;


command : command_block	 	{ $$ = $1 }
	| flow_control 		{ $$ = $1 }
	| atrib 			{ $$ = $1 }
	| input				{ $$ = $1 }
	| output			{ $$ = $1 }
	| return 		{ $$ = $1 }
	|
	;

atrib : TK_IDENTIFICADOR '=' expr                    { $$ = astCreate(IKS_AST_ATRIBUICAO, $3, NULL, NULL, NULL); }
	| TK_IDENTIFICADOR '[' expr ']' '=' expr     { $$ = astCreate(IKS_AST_ATRIBUICAO, $3, $6, NULL, NULL); }
	;

input : TK_PR_INPUT TK_IDENTIFICADOR           { $$ = astCreate(IKS_AST_INPUT, NULL, NULL, NULL, NULL); }
	;

output : TK_PR_OUTPUT element_list_not_empty   { $$ = astCreate(IKS_AST_OUTPUT, $2, NULL, NULL); }
	;

element_list_not_empty : expr   { $$ = astCreate(IKS_AST_OUTPUT, $1, NULL, NULL, NULL); }
	;

return : TK_PR_RETURN expr   			{ $$ = astCreate(IKS_AST_RETURN, $2, NULL, NULL, NULL); }
	;

flow_control : TK_PR_IF '(' expr ')' TK_PR_THEN command      { $$ = astCreate(IKS_AST_IF_ELSE , $3, $6, NULL, NULL); }
	| TK_PR_IF '(' expr ')' TK_PR_THEN command TK_PR_ELSE command  
							     { $$ = astCreate(IKS_AST_IF_ELSE , $3, $6, $8, NULL); }
	| TK_PR_WHILE '(' expr ')' TK_PR_DO command  { $$ = astCreate(IKS_AST_WHILE_DO, $3, $6, NULL, NULL); }
	| TK_PR_DO command TK_PR_WHILE '(' expr ')'  { $$ = astCreate(IKS_AST_DO_WHILE, $2, $5, NULL, NULL); }
	;

expr : 
	| expr '+' expr                            { $$ = astCreate(IKS_AST_ARIM_SOMA, $1, $3, NULL, NULL); }
	| expr '-' expr                            { $$ = astCreate(IKS_AST_ARIM_SUBTRACAO, $1, $3, NULL, NULL); }
	| expr '*' expr                            { $$ = astCreate(IKS_AST_ARIM_MULTIPLICACAO, $1, $3, NULL, NULL); }
	| expr '/' expr                            { $$ = astCreate(IKS_AST_ARIM_DIVISAO, $1, $3, NULL, NULL); }
	| expr '<' expr                            { $$ = astCreate(IKS_AST_LOGICO_COMP_L, $1, $3, NULL, NULL); }
	| expr '>' expr                            { $$ = astCreate(IKS_AST_LOGICO_COMP_G, $1, $3, NULL, NULL); }
	| '(' expr ')'                             { $$ = $2 }
	| expr TK_OC_LE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_LE, $1, $3, NULL, NULL); }
	| expr TK_OC_GE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_GE, $1, $3, NULL, NULL); }
	| expr TK_OC_EQ expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_IGUAL, $1, $3, NULL, NULL); }
	| expr TK_OC_NE expr                       { $$ = astCreate(IKS_AST_LOGICO_COMP_DIF, $1, $3, NULL, NULL); }
	| expr TK_OC_AND expr                      { $$ = astCreate(IKS_AST_LOGICO_E, $1, $3, NULL, NULL); }
	| expr TK_OC_OR expr                       { $$ = astCreate(IKS_AST_LOGICO_OU, $1, $3, NULL, NULL); }
	| TK_IDENTIFICADOR '(' expression_list ')' { $$ = astCreate(IKS_AST_CHAMADA_DE_FUNCAO, $3, NULL, NULL, NULL); }
	| TK_IDENTIFICADOR			               { $$ = astCreate(IKS_AST_IDENTIFICADOR, NULL, NULL, NULL, NULL); }
	| TK_IDENTIFICADOR '[' expr ']'            { $$ = astCreate(IKS_AST_VETOR_INDEXADO, $3, NULL, NULL, NULL); }
	| TK_LIT_INT                               { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_FLOAT                             { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); } 
	| TK_LIT_FALSE                             { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_TRUE                              { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_CHAR                              { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	| TK_LIT_STRING                            { $$ = astCreate(IKS_AST_LITERAL, NULL, NULL, NULL, NULL); }
	|
	;

expression_list : expression_list_not_empty
	|
	;

expression_list_not_empty : expr ';' expression_list_not_empty
	| expr
	;

%%
