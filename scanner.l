/* Grupo: Márcio Mello da Silva, Lisardo Sallaberry Kist, Mathieu Lienard Mayor */
%{
#include "comp_dict.h"
#include "comp_tree.h"
#include <stdlib.h>
#include <stdio.h>
#include "parser.h" //arquivo automaticamente gerado pelo bison
int running;
int dummy_type;
int dummy_line;
int dummy_key;


/* Essa são as funções de manipulação de String */
#define STRINGBUF_CHUNK 16
char *stringbuf = NULL;
int stringbuf_pos;
int stringbuf_size;


void printSymbolTable() {
    print_dict(symbol_table);
}

/* Reseta */
void stringbuf_reset(void) {
	// free(stringbuf);
	stringbuf = NULL;
	stringbuf_pos = 0;
	stringbuf_size = 0;
}

/* Põe um caracter */
void stringbuf_push(char ch) {
	if (stringbuf_size == stringbuf_pos)
		stringbuf = realloc(stringbuf, stringbuf_size += STRINGBUF_CHUNK);
	stringbuf[stringbuf_pos++] = ch;
}

/* Macro para adição de tokens. Devia estar indo para uma tabela de simbolos */
#define add_token(lextype, lexkey, lexvalue) do { \
	char* str; \
	str = (char*)malloc(10*sizeof(char)); \
	symbol_table = dict_insert (symbol_table, lexkey, str); \
} while (0)


%}

%x COMMENT
%x STRING

%%
int     { return TK_PR_INT; }
float   { return TK_PR_FLOAT; }
bool    { return TK_PR_BOOL; }
char	{ return TK_PR_CHAR; }
string  { return TK_PR_STRING; }
if      { return TK_PR_IF; }
then    { return TK_PR_THEN; }  
else    { return TK_PR_ELSE; }
while   { return TK_PR_WHILE; }
do      { return TK_PR_DO; }
input   { return TK_PR_INPUT; }
output  { return TK_PR_OUTPUT; }
return  { return TK_PR_RETURN; }


" "     {}
"\t"    {}
"\n"    { current_line++; }

"//".*        {}
"/*"          { BEGIN(COMMENT); }
<COMMENT>"*/" { BEGIN(INITIAL); }
<COMMENT>"\n" { current_line++; }
<COMMENT>.    {}

[][<=>+*/%&$(){},;:!-] { return yytext[0]; }
"=="    { return TK_OC_EQ; }
"!="    { return TK_OC_NE; }
"<="    { return TK_OC_LE; }
">="    { return TK_OC_GE; }
"&&"    { return TK_OC_AND;}
"||"    { return TK_OC_OR; }




[0-9]*   {
	return TK_LIT_INT; 
}

true    { return TK_LIT_TRUE;  }
false   { return TK_LIT_FALSE; }

[0-9]+("."[0-9]+)?([eE][+-]?[0-9]+)? {
	return TK_LIT_FLOAT; 
}

"\"" { 
	stringbuf_reset();
	BEGIN(STRING);
	return TK_LIT_STRING;
}

<STRING>[^\\\n"] { stringbuf_push(yytext[0]); }
<STRING>"\n"  { stringbuf_push('\n'); current_line++; }
<STRING>"\""  {
	BEGIN(INITIAL);
	add_token(TK_LIT_STRING, stringbuf, stringbuf); 
}

<STRING>"\\". {
	int ch = yytext[1];
	if (ch == TOKEN_ERRO) {
		printf("Unknown sequence\n");
		return TOKEN_ERRO;
	}
	else
		stringbuf_push(ch);
}


\'.\'   { return TK_LIT_CHAR; }


[A-Za-z_][A-Za-z_0-9]* {
	return TK_IDENTIFICADOR; 
}



.	{ printf("%d TOKEN_ERRO [%s]", current_line, yytext);  }
%%

void initMe(void) {
	running = 1;
	current_line = 0;
	symbol_table = dict_create();
}

int yywrap(void) {
	running = 0;
	return 1;
}
